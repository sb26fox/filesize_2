﻿using Domain;
using Infrastructure;
using Moq;
using NUnit.Framework;

namespace FileManagerTest
{
    [TestFixture]
    public class FileManagerShould
    {
        [SetUp]
        public void SetUp()
        {
            fileSystemService = new Mock<IFileSystemService>();
            manager = new FileManager(fileSystemService.Object);
        }

        private Mock<IFileSystemService> fileSystemService;
        private FileManager manager;

        [Test]
        public void ReturnFileSize_IfFileExists()
        {
            fileSystemService.Setup(x => x.IsExists("@D:\\input.txt")).Returns(true);
            fileSystemService.Setup(x => x.GetSize("@D:\\input.txt")).Returns(10);

            var fileSize = manager.GetFileSize("@D:\\input.txt");

            Assert.AreEqual(10, fileSize);
        }

        [Test]
        public void ReturnTrue_IfFileExists()
        {
            fileSystemService.Setup(x => x.IsExists("@D:\\input.txt")).Returns(true);

            var isExists = manager.FileExists("@D:\\input.txt");

            Assert.IsTrue(isExists);
        }

        [Test]
        public void ReturnZero_IfFileNotExists()
        {
            fileSystemService.Setup(x => x.IsExists("@D:\\input.txt")).Returns(false);

            var fileSize = manager.GetFileSize("@D:\\input.txt");

            Assert.AreEqual(0, fileSize);
        }

        [Test]
        public void ReturnZero_IfPath_IsNullOrEmpty()
        {
            var fileSize = manager.GetFileSize(string.Empty);

            Assert.AreEqual(0, fileSize);
        }
    }
}