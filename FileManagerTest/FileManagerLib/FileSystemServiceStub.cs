﻿using Infrastructure;

namespace Domain
{
    public class FileSystemServiceStub : IFileSystemService
    {
        public bool IsExists(string path)
        {
            if (path == "input.txt")
            {
                return true;
            }

            return false;
        }

        public long GetSize(string path)
        {
            if (path == "input.txt")
            {
                return 10;
            }

            return 0;
        }
    }
}