﻿using Domain;
using Infrastructure;
using NUnit.Framework;

namespace TestsWithManualStub
{
    [TestFixture]
    public class WhenFileManagerGetSize
    {
        private IFileSystemService fileSystemService;
        private FileManager manager;

        [SetUp]
        public void SetUp()
        {
            fileSystemService = new FileSystemServiceStub();
            manager = new FileManager(fileSystemService);            
        }

        [Test]
        public void ReturnFileSize_IfFileExists()
        {
            var fileSize = manager.GetFileSize("input.txt");

            Assert.AreEqual(10, fileSize);
        }

        [Test]
        public void ReturnTrue_IfFileExists()
        {
            var isExists = manager.FileExists("input.txt");

            Assert.IsTrue(isExists);
        }

        [Test]
        public void ReturnZero_IfFileNotExists()
        {

            var fileSize = manager.GetFileSize("notExistsFile.txt");

            Assert.AreEqual(0, fileSize);
        }

        [Test]
        public void ReturnZero_IfPath_IsNullOrEmpty()
        {
            var fileSize = manager.GetFileSize(string.Empty);

            Assert.AreEqual(0, fileSize);
        }
    }
}