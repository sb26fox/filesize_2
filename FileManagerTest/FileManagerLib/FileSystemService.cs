﻿using System.IO;
using Infrastructure;

namespace Domain
{
    public class FileSystemService : IFileSystemService
    {
        public bool IsExists(string path)
        {
            return File.Exists(path);
        }

        public long GetSize(string path)
        {
            var file = new FileInfo(path);

            return file.Length;
        }
    }
}