﻿namespace Infrastructure
{
    public interface IFileSystemService
    {
        bool IsExists(string path);
        long GetSize(string path);
    }
}