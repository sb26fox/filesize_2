﻿using Infrastructure;

namespace Domain
{
    public class FileManager
    {
        private readonly IFileSystemService service;

        public FileManager()
        {
            service = new FileSystemService();
        }

        public FileManager(IFileSystemService service)
        {
            this.service = service;
        }

        public bool FileExists(string path)
        {
            return service.IsExists(path);
        }

        public double GetFileSize(string path)
        {
            if (service.IsExists(path))
            {
                return service.GetSize(path);
            }

            return 0;
        }
    }
}